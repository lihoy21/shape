# shape

## Полная сборка
```
mvn clean install
```

## Тесты
```
mvn test -Dtestname
```

## Отчет в JaCoCo
```
mvn jacoco:report
```

## Мутационные тесты (pitest) - не забудьте поменять конфигурацию в pom.xml
```
mvn org.pitest:pitest-maven:mutationCoverage
```
